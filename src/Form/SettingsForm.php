<?php

namespace Drupal\inclusive_cards\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Inclusive Cards settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Alias Manager Interface.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $entityDisplayRepository;


  /**
   * The entity type storage.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityDisplayRepository $entityDisplayRepository
   *   The EntityDisplayRepository for entity view modes.
   * @param \Drupal\core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The EntityTypeManagerInterface for entity types.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityDisplayRepository $entityDisplayRepository, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityDisplayRepository = $entityDisplayRepository;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('entity_display.repository'),
    $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'inclusive_cards_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['inclusive_cards.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $entityBundles = $this->getDisplayentityBundlesValues();
    foreach ($entityBundles as $entityBundle) {
      $active_bundles = $this->getActiveEntityBundleDisplayModes($entityBundle['storage_type'], $entityBundle['entity_display']);
      if (is_array($active_bundles) && !empty($active_bundles)) {
        $form[$entityBundle['entity_display']] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Available <strong>@entity_type</strong> display view modes', ['@entity_type' => ucfirst($entityBundle['title'])]),
        ];
        foreach ($active_bundles as $active_bundle_key => $active_bundle_options) {
          $this->buildBundleDisplayModeHeaderList($form, $entityBundle['entity_display'], $active_bundle_key, $active_bundle_options);
        }
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('inclusive_cards.settings')->get('settings');
    $form_state_values = $form_state->getValues();
    $settings['active'] = [];
    foreach ($settings['default'] as $entity_type => $entity_type_data) {
      if (is_array($entity_type_data) && !empty($entity_type_data)) {
        foreach ($entity_type_data as $entity_bundle => $entity_view_modes) {
          if (is_array($entity_view_modes) && !empty($entity_view_modes) > 0) {
            $view_modes = array_keys($entity_view_modes);
            foreach ($view_modes as $view_mode) {
              $settings['active'][$entity_type][$entity_bundle][$view_mode]['img'] = 0;
              $form_state_value = $entity_type . '___' . $entity_bundle . '___' . $view_mode;
              if (isset($form_state_values[$form_state_value]) && !empty($form_state_values[$form_state_value])) {
                $settings['active'][$entity_type][$entity_bundle][$view_mode]['header'] = $form_state_values[$form_state_value];
              }
              if (isset($form_state_values[$form_state_value . '__img']) && !empty($form_state_values[$form_state_value . '__img'])) {
                $settings['active'][$entity_type][$entity_bundle][$view_mode]['img'] = $form_state_values[$form_state_value . '__img'];
              }

            }
          }
        }
      }
    }
    $this->config('inclusive_cards.settings')
      ->set('settings', $settings)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Get the active node bundles (content types) and their display modes.
   *
   * Returns an array of node bundles (content types) and their display modes.
   */
  private function getActiveEntityBundleDisplayModes(String $entity_storage, String $entity_display) : array {

    // Get all node bundles (content types).
    $entity_bundles = $this->entityTypeManager->getStorage($entity_storage)->loadMultiple();
    $exclude_view_modes = ['token', 'full', 'rss', 'default'];
    $settings = $this->config('inclusive_cards.settings')->get('settings');
    // Loop through each node bundle to get active display modes.
    foreach ($entity_bundles as $bundle_name => $bundle_info) {

      $viewmodes = $this->entityDisplayRepository->getViewModeOptionsByBundle(
          $entity_display,
          $bundle_name
        );

      $viewmodes_clean = array_diff(array_keys($viewmodes), $exclude_view_modes);
      $active_view_modes = [];
      foreach ($viewmodes_clean as $viewmodes_clean_key) {
        $active_view_modes[$viewmodes_clean_key] = $viewmodes[$viewmodes_clean_key];
      }
      $entity_bundles[$bundle_name] = $active_view_modes;
    }
    $settings['default'][$entity_display] = $entity_bundles;
    $this->config('inclusive_cards.settings')->set('settings', $settings)->save();
    return $entity_bundles;

  }

  /**
   * Get the entity bundles (content types) and their display modes.
   */
  private function buildBundleDisplayModeHeaderList(array &$form, $entity_type, $bundle, $options) {
    $settings = $this->config('inclusive_cards.settings')->get('settings');

    foreach ($options as $option_key => $option_value) {
      $form_element_name = $entity_type . '___' . $bundle . '___' . $option_key;
      $default_value_header = $settings['active'][$entity_type][$bundle][$option_key]['header'] ?? '';
      $default_value_img = $settings['active'][$entity_type][$bundle][$option_key]['img'] ?? '';

      $form[$entity_type][$bundle][$form_element_name] = [
        '#type' => 'select',
        '#options' => $this->getHeaders(),
        '#default_value' => $default_value_header,
        '#empty_option' => $this->t('-select-'),
        '#attributes' => [
          'data-entity-type' => $entity_type,
          'data-entity-bundle' => $bundle,
          'data-entity-display' => $option_key,
        ],
        '#title' => $this->t('@bundle > @option_value',
          [
            '@bundle' => (string) ucfirst($bundle),
            '@option_value' => $option_value,
          ]),
      ];

      $form[$entity_type][$bundle][$form_element_name . '__img'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Main image of Display Mode <strong>@bundle</strong>/<strong>@option_key</strong> is decorative (sets alt="" and aria-hidden="true"). Select only if you know what you are doing!',
        [
          '@bundle' => $bundle,
          '@option_key' => $option_key,
        ]),
        '#description' => $this->t('Screenreaders will ignore images with aria-hidden="true".'),
        '#type' => 'checkbox',
        '#default_value' => $default_value_img,
        '#attributes' => [
          'data-entity-type' => $entity_type,
          'data-entity-bundle' => $bundle,
          'data-entity-display' => $option_key,
        ],
      ];
    }
  }

  /**
   * Get the possible header teaser elements.
   */
  private function getHeaders() :array {
    return [
      'h2' => 'h2 > a',
      'h3' => 'h3 > a',
      'h4' => 'h4 > a',
      'h5' => 'h5 > a',
    ];
  }

  /**
   * Get the entity bundles and their display modes.
   */
  private function getDisplayentityBundlesValues() {
    return [
      'node' => [
        'storage_type' => 'node_type',
        'entity_display' => 'node',
        'title' => $this->t('Node'),
      ],
      'taxonomy_term' => [
        'storage_type' => 'taxonomy_vocabulary',
        'entity_display' => 'taxonomy_term',
        'title' => $this->t('Taxonomy Vocabulary'),
      ],
    ];

  }

}
