(function (Drupal, once) {
  Drupal.behaviors.inclusiveCard = { // Changed behavior name to avoid collisions
    attach: function (context) {
      let cards = once(
        "inclusive-card",
        context.querySelectorAll(".inclusive-card")
      );
      cards.forEach(cardInclusive);
    },
  };

  /**
   * Enhances a card element by adding a click event to it if it contains a link.
   *
   * @param {HTMLElement} card - The card element to enhance.
   * @returns {void}
   */
  function cardInclusive(card) {
    // Validate inputs
    if (!(card instanceof HTMLElement)) {
      console.error("Invalid card element:", card);
      return;
    }

    const { inclusiveCardHeader, inclusiveCardImg, historyNodeId } = card.dataset;

    // Check if inclusiveCardHeader is defined and not "undefined"
    if (!inclusiveCardHeader || inclusiveCardHeader === "undefined") {
      console.error("Missing or invalid inclusiveCardHeader for card:", card);
      return;
    }

    const link = card.querySelector(`${inclusiveCardHeader} a`);
    if (!link) {
      console.error("Link not found within card:", card);
      return;
    }

    if (inclusiveCardImg === "1") {
      const img = card.querySelector('img');
      if (!img) {
        console.error(`img is undefined for card with nid: ${historyNodeId}`);
      } else {
        img.setAttribute('alt', '');
        img.setAttribute('aria-hidden', 'true');
      }
    }

    card.addEventListener('click', (event) => {
      if (!event.detail || event.detail === 1) {
        link.click();
      }
    });

    card.style.cursor = "pointer";
  }
})(Drupal, once);
